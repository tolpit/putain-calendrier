"use strict";


function getMonday(d) {
  d = new Date(d);
  var day = d.getDay(),
      diff = d.getDate() - day + (day == 0 ? -6:1); // adjust when day is sunday
  return new Date(d.setDate(diff));
}

class Dispatcher {
	
	constructor() {
		this.events = {};
	}

	on(name, callback) {
		if(!this.events[name]) this.events[name] = [];

		this.events[name].push(callback);
	}

	emit(name) {
		if(!this.events[name] || !this.events[name].length) return;
	
		this.events[name].forEach(callback => {
			if(typeof callback === 'function') callback();
		});
	}
}

class Monitor extends Dispatcher {

	constructor() {
		super();

		this.isIdle 		= true;
		this.timeout 		= 2 * 1000;
		this.eventsToListen = 'mousemove keydown mousedown touchstart touchmove';
		//DOMMouseScroll mousewheel

		this.attachEvents();
	}

	attachEvents() {
		this.eventsToListen.split(' ').forEach(eventName => {
			document.addEventListener(eventName, this.handleActive.bind(this), false);
		});
	}

	handleActive() {
		requestAnimationFrame(() => {
			if(this.timer) {
				clearTimeout(this.timer);
				this.timer = null;
			}

			this.isIdle = false;

			this.emit('active');

			this.timer = setTimeout(this.handleIdle.bind(this), this.timeout);
		});
	}

	handleIdle() {
		if(this.isIdle === false) {
			this.isIdle = true;

			this.emit('idle');
		}
	}
}