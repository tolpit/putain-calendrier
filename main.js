const now = new Date(2018,01,01);

const year = now.getFullYear();

const months = ['janvier', 'février', 'mars', 'avril', 'mai', 'juin', 'juillet', 'août', 'septembre', 'octobre', 'novembre', 'décembre']

const putains = [
	"Putain de \nGalette des rois",
	"Putain \nde Froid",
	"Putain \nde Grippe",
	"Putain \nde Poisson",
	"Putain \nd'Impôts",
	"Putain \nd'Examens",
	"Putain \nde Défilé",
	"Putain \nde Touristes",
	"Putain \nde Rentrée",
	"Putain d'Heure \nd'Hiver",
	"Putain \nde Grisaille",
	"Putain de \nCadeaux de Noël",
];

const data = {

	isIdle: true,

	menuOpen: false,

	year,

	items: Array.from({ length: 12 }).map((e, index) => {
		const firstDayOfTheMonth = new Date(year, index, 1);
		const lastDayOfTheMonth  = new Date(year, index, 0);

		let paddingDays = 0;
		let day 		= getMonday(firstDayOfTheMonth);
		let mondayDay   = getMonday(firstDayOfTheMonth);

		while(day.getTime() !== firstDayOfTheMonth.getTime()) {
			paddingDays++;

			day = new Date(day.setDate(day.getDate() + 1))
		}

		// console.log({mondayDay,day,firstDayOfTheMonth,paddingDays});

		// while(firstDay.getDay() !== 0 || firstDay.getDay() !== 1) {
		// 	paddingDays++;

		// 	firstDay = new Date(year, index, -paddingDays);

		// 	console.log({firstDay});
		// }

		// console.log(firstDayOfTheMonth);

		return {
			month: months[index],
			putain: putains[index],
			paddingDays,
			days: Array.from({ length: lastDayOfTheMonth.getDate() }).map((e, dayNumber) => {
				dayNumber = dayNumber + 1;
				
				return {
					dayNumber: dayNumber,
					isMonday: new Date(year, index, dayNumber).getDay() === 1
				}
			})
		}
	})

};

var app = new Vue({
	name: "View",
  	el: '#app',
  	data: data,

  	methods: {
  		handleActive() {
  			if(this.isIdle) this.isIdle = false;
		},
  		handleIdle() {
  			if(!this.isIdle) this.isIdle = true;

  			this.menuOpen = false;
  		}
  	},

  	created() {
  		this.monitor = new Monitor();
  		this.monitor.on('idle', this.handleIdle.bind(this));
  		this.monitor.on('active', this.handleActive.bind(this));
  	}

});